import csv

from flask import render_template, app, request, Response
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder import ModelView, ModelRestApi, IndexView
from flask_wtf import CSRFProtect
from six import StringIO

from app.models import Aco
from . import appbuilder, db, app

class Acos(ModelView):
    datamodel = SQLAInterface(Aco)
    list_title = "ACOs list"
    show_title = "ACO details"
    add_title = "Add ACO"
    edit_title = "Edit ACO"
    label_columns = {'a': 'a','e': 'e','i': 'i','packed': 'Packed name', 'MOID': 'MOID', 'acotype': 'Type', 'ra': 'RA',
                     'dec': 'DEC'}
    list_columns = ['name', 'packed', 'a', 'e', 'i', 'H', 'G', 'T', 'MOID', 'acotype', 'ra', 'dec', 'v']


appbuilder.add_view(Acos, "ACOs list", icon="fa-folder-open-o", category="Database", category_label="Menu")


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return (
        render_template(
            "404.html", base_template=appbuilder.base_template, appbuilder=appbuilder
        ),
        404,
    )

csrf = CSRFProtect()
@csrf.exempt
@app.route('/api/create_acos', methods=['POST'])
def create_acos():
    try:
        db.session.query(Aco).delete()

        jacos = request.get_json()['acos']

        for jaco in jacos:
            aco = Aco()
            aco.a = jaco['a']
            aco.e = jaco['e']
            aco.i = jaco['i']
            aco.H = jaco['H']
            aco.G = jaco['G']
            aco.T = jaco['T']
            aco.MOID = jaco['MOID']
            aco.acotype = jaco['acotype']
            aco.ra = jaco['ra']
            aco.dec = jaco['dec']
            aco.v = jaco['v']
            aco.packed = jaco['packed']
            aco.name = jaco['name']
            aco.internalID = jaco['internalID']
            db.session.add(aco)
            db.session.commit()

        return 'success'

    except Exception as e:
        return 'error: ' + str(e)


def generatecsv():
    data = StringIO()
    w = csv.writer(data)
    w.writerow(('Name','Packed name','a','e','i','H','G','T','MOID','Type','RA','DEC','V'))
    yield data.getvalue()
    data.seek(0)
    data.truncate(0)
    for a in db.session.query(Aco):
        w.writerow((a.name, a.packed, a.a, a.e, a.i, a.H, a.G, a.T, a.MOID, a.acotype, a.ra, a.dec, a.v))
        yield data.getvalue()
        data.seek(0)
        data.truncate(0)

csrf = CSRFProtect()
@csrf.exempt
@app.route('/api/download_csv', methods=['GET'])
def downloadcsv():
    response = Response(generatecsv(), mimetype='text/csv')
    response.headers.set("Content-Disposition", "attachment", filename="acos.csv")
    return response



db.create_all()