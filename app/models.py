from flask_appbuilder import Model
from sqlalchemy import Column, Integer, String, ForeignKey, Float


class Aco(Model):
    id = Column(Integer, primary_key=True)

    internalID = Column(Integer, unique=True, nullable=False)
    name = Column(String(20), unique=True, nullable=False)
    packed = Column(String(20), unique=True, nullable=False)
    a = Column(Float, unique=False, nullable=False)
    e = Column(Float, unique=False, nullable=False)
    i = Column(Float, unique=False, nullable=False)
    H = Column(Float, unique=False, nullable=False)
    G = Column(Float, unique=False, nullable=False)
    T = Column(Float, unique=False, nullable=False)
    MOID = Column(Float, unique=False, nullable=False)
    acotype = Column(String(10), unique=False, nullable=False)
    ra = Column(Float, unique=False, nullable=False)
    dec = Column(Float, unique=False, nullable=False)
    v = Column(Float, unique=False, nullable=False)

    notes = Column(String(255), unique=False, nullable=True)





