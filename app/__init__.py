import logging

from flask import Flask
from flask_appbuilder import AppBuilder, SQLA, IndexView

class CustomIndexView(IndexView):
    index_template = 'index.html'

logging.basicConfig(format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")
logging.getLogger().setLevel(logging.DEBUG)

app = Flask(__name__)
app.config.from_object("config")
db = SQLA(app)
appbuilder = AppBuilder(app, db.session, indexview=CustomIndexView)


from . import views
