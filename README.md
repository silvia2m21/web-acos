#Web-acos
  
##Setup & Run

1 - Create virtual environment (optional)

`
python3 -m venv venv
`

2 - Activate virtual environment (optional)

`
source venv/bin/activate
`

3- Install dependencies

`
pip3 install -r requirements.txt
`

4 - Run it

`
pyton3 run.py
`

5 - Open in browser

`
localhost:8080
`
